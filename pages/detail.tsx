import {loadStarshipDetail, MyAppState, Starship, WithMyStore} from "../common/store";
import * as React from "react";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {NextContext} from "next";
import Link from "next/link";

const formatCost = (cost: string): string => cost.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

interface DetailPageProps {
    readonly detail?: Starship;
    readonly loadStarship: (id: string) => void;
}

class DetailPage extends React.Component<DetailPageProps, {}> {

    static async getInitialProps({store, query}: NextContext & WithMyStore) {
        await store.dispatch(loadStarshipDetail(`${query.id}`) as any);
    }

    render () {
        const {detail} = this.props;

        if (!detail) {
            return <div>loading...</div>
        }

        return (
            <div className="detail">
                <h1>{detail.name}</h1>
                <p><strong>{detail.model}</strong></p>
                <p>{detail.manufacturer}</p>
                <p>Price: {formatCost(detail.cost)} credits</p>
                <Link href="/">
                    <a>Back to list</a>
                </Link>
            </div>
        );
    }
}

export default connect(
    ({detail}: MyAppState) => ({detail}),
    (dispatch: Dispatch) => bindActionCreators({loadStarshipDetail}, dispatch)
)(DetailPage);
