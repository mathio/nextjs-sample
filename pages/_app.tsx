import "./_app.scss";
import App, {Container, NextAppContext} from "next/app";
import Head from "next/head";
import React from "react";
import withRedux from "next-redux-wrapper";
import {makeStore, MyStore} from "../common/store";
import {Provider} from "react-redux";

interface MyAppProps {
    readonly store: MyStore;
}

class MyApp extends App<MyAppProps> {
    static async getInitialProps ({Component, ctx}: NextAppContext) {
        let pageProps: any = {};

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx)
        }

        return {
            pageProps
        };
    }

    render () {
        const {Component, pageProps, store} = this.props;

        return (
            <Container>
                <Head>
                    <title>Starships</title>
                </Head>
                <Provider store={store}>
                    <Component {...pageProps} />
                </Provider>
            </Container>
        );
    }
}

export default withRedux(makeStore)(MyApp);