import * as React from "react";
import "isomorphic-fetch";
import {loadStarships, loadMoreStarships, MyAppState, Starship, WithMyStore} from "../common/store";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import Link from "next/link";

interface IndexPageProps {
    readonly list: Starship[];
    readonly loadStarships: () => void;
    readonly loadMoreStarships: () => void;
}

class IndexPage extends React.Component<IndexPageProps, {}> {

    static async getInitialProps({store}: WithMyStore) {
        await store.dispatch(loadStarships() as any);
    }

    async loadNextPage() {
        await this.props.loadMoreStarships();
    }

    render () {
        return (
            <div>
                <h1>Starships</h1>
                <ul>
                    {this.props.list.map(item => (
                        <li key={item.name}>
                            <Link href={`/detail?id=${item.id}`}>
                                <a>{item.name}</a>
                            </Link> (by {item.manufacturer})
                        </li>
                    ))}
                </ul>
                <a onClick={() => this.loadNextPage()}>show more</a>
            </div>
        );
    }
}

export default connect(
    ({list}: MyAppState) => ({list}),
    (dispatch: Dispatch) => bindActionCreators({loadMoreStarships}, dispatch)
)(IndexPage);
