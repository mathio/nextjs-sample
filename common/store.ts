import {createStore, applyMiddleware, Action, Dispatch, Store} from "redux";
import thunk from 'redux-thunk';
import {fetchStarshipDetail, fetchStarships} from "./serverApi";


export interface Starship {
    readonly id: string;
    readonly name: string;
    readonly model: string;
    readonly manufacturer: string;
    readonly cost: string;
}

export interface MyAppState {
    readonly list: Starship[];
    readonly detail?: Starship;
    readonly page: number;
}

interface MyAction<T> extends Action<string> {
    readonly payload: T;
}

export type MyStore = Store<MyAppState, MyAction<any>>;

export interface WithMyStore {
    readonly store: MyStore
}

const reducer = (state: MyAppState = {list: [], page: 1}, action: Action) => {

    const {type, payload} = action as MyAction<any>;

    console.log("DISPATCH:", type, payload);    // logging for dispatched actions

    switch (type) {
        case "SET_STARSHIPS":
            return {...state, list: payload, page: 1};
        case "SET_MORE_STARSHIPS":
            const list = [...state.list, ...payload];
            return {...state, list, page: state.page + 1};
        case "SET_STARSHIP_DETAIL":
            return {...state, detail: payload};
        default:
            return state;
    }
};

export const loadStarships = () => async (dispatch: Dispatch<MyAction<any>>, getState: () => MyAppState) =>  {
    const {list} = getState();

    if (list.length === 0) {
        const data = await fetchStarships(1);

        if (data.length > 0) {
            dispatch({type: "SET_STARSHIPS", payload: data});
        }
    }
};

export const loadMoreStarships = () => async (dispatch: Dispatch<MyAction<any>>, getState: () => MyAppState) =>  {
    const {page} = getState();
    const data = await fetchStarships(page + 1);

    if (data.length > 0) {
        dispatch({type: "SET_MORE_STARSHIPS", payload: data});
    }
};

export const loadStarshipDetail = (id: string) => async (dispatch: Dispatch<MyAction<any>>) =>  {
    const data = await fetchStarshipDetail(id);
    dispatch({type: "SET_STARSHIP_DETAIL", payload: data});
};


export const makeStore = (initialState?: MyAppState) => {
    return createStore(reducer, initialState, applyMiddleware(thunk));
};
