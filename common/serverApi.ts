import fetch from "isomorphic-fetch";
import {Starship} from "./store";

const mapApiResultToStarship = ({name, model, manufacturer, cost_in_credits, url}: any): Starship => {
    const matches = url.match(/\/([a-z0-9]+)\/$/i);
    return {
        id: matches && matches.length > 1 ? matches[1] : null,
        name,
        model,
        manufacturer,
        cost: cost_in_credits
    };
};

export const fetchStarships = async (page: number): Promise<Starship[]> => {
    const response = await fetch(`http://localhost:3000/api/starships/?format=json&page=${page}`);
    const starships = await response.json();
    if (starships && starships.results) {
        return starships.results.map(mapApiResultToStarship);
    } else {
        return [] as Starship[];
    }
};

export const fetchStarshipDetail = async (id: string): Promise<Starship> => {
    const response = await fetch(`http://localhost:3000/api/starships/${id}/?format=json`);
    const starship = await response.json();
    return mapApiResultToStarship(starship);
};
