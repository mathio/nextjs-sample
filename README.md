# nextjs-samle

This is a sample nextjs app with:

* typescript
* sass for css styles
* redux store for data
* thunk for async side effects

To run:

* yarn install
* yarn start
* open http://localhost:3000/
