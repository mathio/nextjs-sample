const {createServer} = require("http");
const {parse} = require("url");
const next = require("next");
const httpProxy = require("http-proxy");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

const apiProxy = httpProxy.createProxyServer();
const proxyHost = "swapi.co";
const proxyUrl = `https://${proxyHost}`;

const handleApiProxy = (req, res) => {
    console.info(`[API PROXY] ${req.method}: ${req.url}`);
    req.headers.host = proxyHost;
    apiProxy.web(req, res, {target: proxyUrl}, (err) => {
        console.error("[API PROXY] Error:", err);
        res.statusCode = 500;
        res.end(JSON.stringify({error: "Error at proxy"}));
    });
};

app.prepare().then(() => {
    createServer((req, res) => {
        const parsedUrl = parse(req.url, true);
        const { pathname } = parsedUrl;

        if (/^\/api\//.test(pathname)) {
            handleApiProxy(req, res);
        } else {
            handle(req, res, parsedUrl);
        }
    }).listen(3000, err => {
        if (err) {
            throw err;
        }
        console.log("> Ready on port 3000")
    })
});
